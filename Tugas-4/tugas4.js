//soal 1

var angka = 1;
console.log("LOOPING PERTAMA");
while (angka <= 20) {
  if (angka % 2 == 0) {
    console.log(angka + " - I love coding");
  }
  angka++;
}

var nilai = 20;
console.log("LOOPING KEDUA");
while (nilai >= 2) {
  if (nilai % 2 == 0) {
    console.log(nilai + " - I will become a frontend developer");
  }
  nilai--;
}

//soal 2
var angka = 1;
for (angka = 1; angka <= 20; angka++) {
  if (angka % 3 == 0 && angka % 2 == 1) {
    console.log(angka + " - I Love Coding");
  } else if (angka % 2 == 0) {
    console.log(angka + " - Berkualitas");
  } else if (angka % 2 == 1) {
    console.log(angka + " - Santai");
  }
}

//soal3

for (var baris = 1; baris <= 7; baris++) {
  var a = "";
  for (var nilai = 1; nilai <= baris; nilai++) {
    var a = a + "#";
  }
  console.log(a);
}

//soal 4
var kalimat = "saya sangat senang belajar javascript";
var split = kalimat.split(" ");
console.log(split);
//soal 5
var daftarBuah = [
  "2. Apel",
  "5. Jeruk",
  "3. Anggur",
  "4. Semangka",
  "1. Mangga",
];
var buahSort = daftarBuah.sort();
buahJoin = buahSort.join("\n");
console.log(buahJoin);
