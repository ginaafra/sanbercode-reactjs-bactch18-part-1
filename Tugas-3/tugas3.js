//soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";
var upperCase = kataKeempat.toUpperCase();
console.log(
  kataPertama +
    " ".concat(kataKedua.charAt(0).toUpperCase()).concat(kataKedua.slice(1)) +
    " ".concat(kataKetiga) +
    " ".concat(upperCase)
);

//soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "3";
var kataKeempat = "4";

var kataPertama1 = Number(kataPertama);
var kataKedua1 = Number(kataKedua);
var kataKetiga1 = Number(kataKetiga);
var kataKeempat1 = Number(kataKeempat);
var jumlah = kataPertama1 + kataKedua1 + kataKetiga1 + kataKeempat1;
console.log(jumlah);

//soal 3
var kalimat = "wah javascript itu keren sekali";
var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);

//soal 4
var nilai = 25;
if (nilai >= 80) {
  console.log("indeks A");
} else if (nilai >= 70 && nilai < 80) {
  console.log("indeks B");
} else if (nilai >= 60 && nilai < 70) {
  console.log("indeks C");
} else if (nilai >= 50 && nilai < 60) {
  console.log("indeks D");
} else {
  console.log("indeks E");
}

//soal 5
var tanggal = 13;
var bulan = 7;
var tahun = 1999;
switch (bulan) {
  case 1: {
    bulan = "Januari";
    break;
  }
  case 2: {
    bulan = "Februari";
    break;
  }
  case 3: {
    bulan = "Maret";
    break;
  }
  case 4: {
    bulan = "April";
    break;
  }
  case 5: {
    bulan = "Mei";
    break;
  }
  case 6: {
    bulan = "Juni";
    break;
  }
  case 7: {
    bulan = "Juli";
    break;
  }
  case 8: {
    bulan = "Agustus";
    break;
  }
  case 9: {
    bulan = "September";
    break;
  }
  case 10: {
    bulan = "Oktober";
    break;
  }
  case 11: {
    bulan = "November";
    break;
  }
  case 12: {
    bulan = "Desember";
    break;
  }
  default: {
    console.log("Tiada bulan");
  }
}
console.log(tanggal + " ".concat(bulan) + " ".concat(tahun));
