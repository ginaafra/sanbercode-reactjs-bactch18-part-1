var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];
var waktuSekarang = 10000;
readBooksPromise(waktuSekarang, books[0]).then((f1) => {
  readBooksPromise(f1, books[1]).then((f2) => {
    readBooksPromise(f2, books[2]).then((f3) => {
      readBooksPromise(f3, books[3])
        .then((f4) => {
          console.log(f4);
        })
        .catch(function (err) {
          console.log(err);
        });
    });
  });
});
