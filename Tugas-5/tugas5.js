//soal 1
function halo() {
  return "Halo Sanbers!";
}

console.log(halo());

//soal 2
function kalikan(a, b) {
  return a * b;
}
var num1 = 12;
var num2 = 4;
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

//soal3
function introduce(name, age, address, hobby) {
  var kalimat =
    "Nama saya " +
    name +
    ", umur saya " +
    age +
    " tahun, alamat saya di " +
    address +
    ", dan saya punya hobby yaitu " +
    hobby;

  return kalimat;
}

var name = "John";
var age = 30;
var address = "Jalan belum jadi";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);

//soal 4
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992];
var objDaftarPeserta = {
  nama: arrayDaftarPeserta[0],
  "jenis kelamin": arrayDaftarPeserta[1],
  hobi: arrayDaftarPeserta[2],
  "tahun lahir": arrayDaftarPeserta[3],
};
console.log(objDaftarPeserta);
//soal 5
var namaBuah = [
  { nama: "strawberry", warna: "merah", "ada bijinya": "ada", harga: 9000 },
  { nama: "jeruk", warna: "oranye", "ada bijinya": "ada", harga: 8000 },
  {
    nama: "semangka",
    warna: "hijau & merah",
    "ada bijinya": "ada",
    harga: 10000,
  },
  { nama: "pisang", warna: "kuning", "ada bijinya": "tidak", harga: 5000 },
];
console.log(namaBuah[0]);

//soal 6
var dataFilm = [];
function addDataFilm(a, b, c, d) {
  dataFilm.push(a, b, c, d);
}
var film = {
  nama: "Adakah CInta",
  durasi: "300 menit",
  genre: "Romance",
  tahun: "2002",
};
var film1 = {
  nama: "Hai",
  durasi: "120 menit",
  genre: "Romance",
  tahun: "2020",
};
addDataFilm(film.nama, film.durasi, film.genre, film.tahun);
addDataFilm(film1.nama, film1.durasi, film1.genre, film1.tahun);

console.log(dataFilm);
