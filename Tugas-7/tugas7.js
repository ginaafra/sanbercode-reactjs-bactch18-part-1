//soal 1
class Animal {
  constructor() {
    this._name = "";
    this._legs = "";
    this._cold_blooded = "";
  }
  get name() {
    return this._name;
  }
  set name(x) {
    this._name = x;
  }
  get legs() {
    return this._legs;
  }
  set legs(x) {
    this._legs = x;
  }
  get cold_blooded() {
    return this._cold_blooded;
  }
  set cold_blooded(x) {
    this._cold_blooded = x;
  }
}
var sheep = new Animal();
sheep.name = "sheep";
sheep.legs = 4;
sheep.cold_blooded = false;
console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);

class Ape extends Animal {
  yell() {
    return "Auooo";
  }
}
var sungokong = new Ape();
sungokong.name = "kera sakti";
sungokong.legs = 2;
console.log(sungokong.yell());
class Frog extends Animal {
  jump() {
    return "hop hop";
  }
}
var kodok = new Frog();
kodok.name = "buduk";
kodok.legs = 2;
console.log(kodok.jump()); // "hop hop"

//soal 2
class Clock {
  constructor({ template }) {
    this._template = template;
    this.timer;
  }
  render() {
    var date = new Date();
    var hours = date.getHours();
    if (hours < 10) hours = "0" + hours;
    var mins = date.getMinutes();
    if (mins < 10) mins = "0" + mins;
    var secs = date.getMinutes();
    if (secs < 10) secs = "0" + secs;

    var output = this._template
      .replace("h", hours)
      .replace("m", mins)
      .replace("s", secs);
    console.log(output);
  }
  stop() {
    clearInterval(this.timer);
  }
  start() {
    this.render.bind();
    this.timer = setInterval(() => this.render(), 1000);
  }
}

var clock = new Clock({ template: "h:m:s" });
clock.start();
