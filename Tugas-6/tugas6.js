//soal1
const luasLingakran = (phi) => {
  let jari2 = 8;
  let luas = phi * jari2 * jari2;
  console.log(luas);
};
const phi = 3.14;
luasLingakran(phi);

const kelilingLingkaran = (phi) => {
  let jari2 = 8;
  let keliling = 2 * phi * jari2;
  console.log(keliling);
};
kelilingLingkaran(phi);

//soal 2
let kalimat = "";
const tambahKalimat = (a, index) => {
  console.log(a);
};
kalimat[0] = tambahKalimat("saya");
kalimat[1] = tambahKalimat("adalah");
kalimat[2] = tambahKalimat("seorang");
kalimat[2] = tambahKalimat("frontend");
kalimat[2] = tambahKalimat("developer");

//soal 3

const literal = (a, b) => {
  const string = `${a} ${b}`;
  console.log(string);
};
literal("WIlliam", "Imoh");
//soal 4
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!",
};
const { firstName, lastName, destination, occupation, spell } = newObject;
console.log(firstName, lastName, destination, occupation);
//soal 5
const west = ["Will", "Chris", "Sam", "Holly"];
const east = [...west, "Gill", "Brian", "Noel", "Maggie"];
console.log(east);
